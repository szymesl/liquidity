//
//  Simulation.metal
//  HelloMetal
//
//  Created by Szymon Litak on 25/03/2017.
//  Copyright © 2017 Razeware LLC. All rights reserved.
//


#include <metal_stdlib>

using namespace metal;


typedef struct
{
    packed_float2 position;
    packed_float2 texCoords;
} VertexIn;

typedef struct {
    float4 position [[position]];
    float2 texCoords;
} FragmentVertex;


vertex FragmentVertex simulation_vertex(device VertexIn *vertexArray [[buffer(0)]],
                                        uint vertexIndex [[vertex_id]])
{
    FragmentVertex out;
    out.position = float4(vertexArray[vertexIndex].position, 0, 1);
    out.texCoords = vertexArray[vertexIndex].texCoords;
    return out;
}





fragment half4 simulation_fragment(FragmentVertex in [[stage_in]],
                                   texture2d<float, access::sample> gameGrid [[texture(0)]],
                                   texture2d<half, access::sample> colorMap [[texture(1)]])
{
    // We sample the game grid to get the value of a cell. The cell is alive
    // if its value is exactly 0; otherwise the value represents the number
    // of simulation steps the cell has been dead. We normalize this to between 0 and 1.
    constexpr sampler nearestSampler(coord::normalized, filter::nearest);
    float deadTime = gameGrid.sample(nearestSampler, in.texCoords).z / 255.0;
    
    // In order to color the simulation, we map the aliveness of a cell onto
    // a color with a 1D texture that contains a gradient.
    half4 color = half4(deadTime); //colorMap.sample(nearestSampler, float2(deadTime, 0));
    return color;
}

kernel void copy(texture2d<float, access::read> readTexture [[texture(0)]],
                 texture2d<float, access::write> writeTexture [[texture(1)]],
                 ushort2 gridPosition [[thread_position_in_grid]]) {
    writeTexture.write(readTexture.read(gridPosition), gridPosition);
}



kernel void laplace(texture2d<float, access::read> x0 [[texture(0)]],
                    texture2d<float, access::read> xn [[texture(1)]],
                    texture2d<float, access::write> xn1 [[texture(2)]],
                    ushort2 gridPosition [[thread_position_in_grid]]
                    ) {
    
    float a = 0.5;
    float c = 1 + 4 * a;
    ushort2 up = ushort2(gridPosition.x, gridPosition.y-1);
    ushort2 left = ushort2(gridPosition.x - 1, gridPosition.y);
    ushort2 right = ushort2(gridPosition.x+1, gridPosition.y);
    ushort2 down = ushort2(gridPosition.x, gridPosition.y+1);
    float4 xx = (x0.read(gridPosition) + a*(xn.read(up)+xn.read(left)+xn.read(right)+xn.read(down)))/c;
    xn1.write(xx, gridPosition);
}

kernel void advect(texture2d<float, access::read> x0 [[texture(0)]],
                   texture2d<float, access::write> x1 [[texture(1)]],
                   ushort2 gridPosition [[thread_position_in_grid]]
                   ) {
    int i0, j0, i1, j1;
    float x, y, s0, t0, s1, t1, dt0;
    
    dt0 = 10; //dt * N
    
    int W = x0.get_width();
    int H = x0.get_height();
    

    x = gridPosition.x-dt0*x0.read(gridPosition).x; y = gridPosition.y-dt0*x0.read(gridPosition).y;
    if (x<0.5f) x=0.5f; if (x>W+0.5f) x=W+0.5f; i0=(int)x; i1=i0+1;
    if (y<0.5f) y=0.5f; if (y>H+0.5f) y=H+0.5f; j0=(int)y; j1=j0+1;
    s1 = x-i0; s0 = 1-s1; t1 = y-j0; t0 = 1-t1;
    x1.write(s0*(t0*x0.read(uint2(i0,j0))+t1*x0.read(uint2(i0,j1)))+
    s1*(t0*x0.read(uint2(i1,j0))+t1*x0.read(uint2(i1,j1))), gridPosition);
//    set_bnd ( N, b, d );
}



kernel void activate_cells(texture2d<float, access::read> x0 [[texture(0)]],
                           texture2d<float, access::write> x1 [[texture(1)]],
                           constant uint2 *cellPositions [[buffer(0)]],
                           ushort2 gridPosition [[thread_position_in_grid]])
{
    ushort cellValue = 1000;
    float2 position = float2(gridPosition);
    uint2 writePosition = cellPositions[gridPosition.x];
    float4 actual = x0.read(writePosition);
    float4 newValue = float4(actual.x, actual.y, actual.z + cellValue, actual.w);
    x1.write(newValue, writePosition);
}

kernel void add_vel_input(constant uint2 *pos [[buffer(0)]],
                          constant float2 *vel [[buffer(1)]],
                          texture2d<float, access::read> x0 [[texture(0)]],
                          texture2d<float, access::write> x1 [[texture(1)]],
                          ushort2 gridPosition [[thread_position_in_grid]])
{
    uint2 xy = pos[gridPosition.x];
    float2 v = vel[gridPosition.x];
    float4 actual = x0.read(xy);
    actual.x += v.x;
    actual.y += v.y;
    x1.write(actual, xy);
}

kernel void pre_project ( texture2d<float, access::read> tx0 [[texture(0)]],
                     texture2d<float, access::write> tx1 [[texture(1)]],
                     ushort2 xy [[thread_position_in_grid]] )
{
    
    ushort2 up = ushort2(xy.x, xy.y-1);
    ushort2 left = ushort2(xy.x - 1, xy.y);
    ushort2 right = ushort2(xy.x+1, xy.y);
    ushort2 down = ushort2(xy.x, xy.y+1);
    
    float4 actual = tx0.read(xy);
    float div = 0.5f * (tx0.read(right).x - tx0.read(left).x + tx0.read(up).x - tx0.read(down).x); // / tx0.get_width()
    actual.x = div;
    tx1.write( actual, xy);
}

kernel void lin_for_project ( texture2d<float, access::read> tx0 [[texture(0)]],
                         texture2d<float, access::write> tx1 [[texture(1)]],
                         ushort2 xy [[thread_position_in_grid]] )
{
    
    ushort2 up = ushort2(xy.x, xy.y-1);
    ushort2 left = ushort2(xy.x - 1, xy.y);
    ushort2 right = ushort2(xy.x+1, xy.y);
    ushort2 down = ushort2(xy.x, xy.y+1);
    
    float4 x0 = tx0.read(xy);
    float lX = (tx0.read(up).x + tx0.read(left).x + tx0.read(right).x + tx0.read(down).x)/4;
    x0.x = lX;
    
    tx1.write(x0, xy);
    
}

kernel void post_project (
                          texture2d<float, access::read> tx0 [[texture(0)]],
                         texture2d<float, access::write> tx1 [[texture(1)]],
                         texture2d<float, access::read> lin_res [[texture(2)]],
                         ushort2 xy [[thread_position_in_grid]] )
{
    
    float4 up = lin_res.read(ushort2(xy.x, xy.y-1));
    float4 left = lin_res.read(ushort2(xy.x - 1, xy.y));
    float4 right = lin_res.read(ushort2(xy.x+1, xy.y));
    float4 down = lin_res.read(ushort2(xy.x, xy.y+1));
    
    float4 actual = tx0.read(xy);
    actual.x -= 0.5 * (right.x - left.x);
    actual.y -= 0.5 * (up.x - down.x);
    tx1.write( actual, xy);
}


kernel void dens_step (texture2d<uint, access::write> textureToShow [[texture(0)]],
                       device float *dens [[buffer(0)]],
                       device float *temp [[buffer(1)]],
                       device float *dens0 [[buffer(2)]],
                       device float *fx [[buffer(3)]],
                       device float *fx0 [[buffer(4)]],
                       device float *fy [[buffer(5)]],
                       device float *fy0 [[buffer(6)]],
                       constant float *dt,
                       constant float *diff,
                       device float *debug,
                       ushort2 gridPosition [[thread_position_in_grid]]) {
    
}



