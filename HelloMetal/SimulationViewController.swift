//
//  SimulationViewController.swift
//  HelloMetal
//
//  Created by Szymon Litak on 25/03/2017.
//  Copyright © 2017 Razeware LLC. All rights reserved.
//

import Foundation
import Metal
import MetalKit


class SimulationViewController: UIViewController, MTKViewDelegate {
    
    var mtkView: MTKView {
        get {
            return self.view as! MTKView
            
        }
    }
    
    let kTextureCount = 4
    var device: MTLDevice! = nil
    var defaultLibrary: MTLLibrary! = nil
    var activationPipelineState: MTLComputePipelineState! = nil
    var addVelPipelineState: MTLComputePipelineState! = nil
    
    var computePipelineState: MTLComputePipelineState! = nil
    var linSolvePipelineState: MTLComputePipelineState! = nil
    var advectPipelineState: MTLComputePipelineState! = nil
    var laplacePipelineState: MTLComputePipelineState! = nil
    var copyPipelineState: MTLComputePipelineState! = nil
    
    var preProjectPipelineState: MTLComputePipelineState! = nil
    var linProjectPipelineState: MTLComputePipelineState! = nil
    var postPorojectPipelineState: MTLComputePipelineState! = nil
    
    
    var renderPipelineState: MTLRenderPipelineState! = nil
    var commandQueue: MTLCommandQueue! = nil
    var timer: CADisplayLink! = nil
    var projectionMatrix: Matrix4!
    var lastFrameTimestamp: CFTimeInterval = 0.0
    
    var textureQueue: [MTLTexture] = []
    
    var vertexBuffer: MTLBuffer! = nil
    
    var densBuffer: MTLBuffer! = nil
    var tempDensBuffer: MTLBuffer! = nil
    var dens0Buffer: MTLBuffer! = nil
    var xForceBuffer: MTLBuffer! = nil
    var xForce0Buffer: MTLBuffer! = nil
    var yForceBuffer: MTLBuffer! = nil
    var yForce0Buffer: MTLBuffer! = nil
    var debugBuffer: MTLBuffer! = nil
    
    
    var currentSimulationStateTexture: MTLTexture!
    var colorMap : MTLTexture!
    var inflightSemaphore: DispatchSemaphore!
    let kMaxInflightBuffers = 3
    var gridSize: MTLSize!
    var samplerState: MTLSamplerState!
    
    var activationPoints: [CGPoint] = []
    var activationVel: [(CGPoint, CGPoint)] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mtkView.device = MTLCreateSystemDefaultDevice()
        device = mtkView.device
        mtkView.delegate = self
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        let pan = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        tap.delegate = self
        pan.delegate = self
        mtkView.addGestureRecognizer(tap)
        mtkView.addGestureRecognizer(pan)
        
        defaultLibrary = device.newDefaultLibrary()
        
        commandQueue = device.makeCommandQueue()
        
        buildComputePipeline()
        buildRenderResources()
        buildRenderPipeline()
        
        mtkView(mtkView, drawableSizeWillChange: CGSize.zero)
        
        inflightSemaphore = DispatchSemaphore(value: kMaxInflightBuffers)
    }
    
    func handleTap(_ sender: UITapGestureRecognizer) {
        let center = sender.location(in: mtkView)
        activationPoints.append(center)
    }
    
    func handlePan(_ sender: UIPanGestureRecognizer) {
        let location = sender.location(in: mtkView)
        activationVel.append((location, sender.velocity(in: mtkView)))
    }
    
    func buildComputePipeline() {
        let computeProgram = defaultLibrary!.makeFunction(name: "calculations")
        
        activationPipelineState = try! self.device.makeComputePipelineState(function: defaultLibrary!.makeFunction(name: "activate_cells")!)
        addVelPipelineState = try! self.device.makeComputePipelineState(function: defaultLibrary!.makeFunction(name: "add_vel_input")!)
        advectPipelineState = try! self.device.makeComputePipelineState(function: defaultLibrary!.makeFunction(name: "advect")!)
        laplacePipelineState = try! self.device.makeComputePipelineState(function: defaultLibrary!.makeFunction(name: "laplace")!)
        preProjectPipelineState = try! self.device.makeComputePipelineState(function: defaultLibrary!.makeFunction(name: "pre_project")!)
        linProjectPipelineState = try! self.device.makeComputePipelineState(function: defaultLibrary!.makeFunction(name: "lin_for_project")!)
        postPorojectPipelineState = try! self.device.makeComputePipelineState(function: defaultLibrary!.makeFunction(name: "post_project")!)

        copyPipelineState = try! self.device.makeComputePipelineState(function: defaultLibrary!.makeFunction(name: "copy")!)
        
        
        
        let samplerDescriptor = MTLSamplerDescriptor()
        samplerDescriptor.sAddressMode = MTLSamplerAddressMode.repeat
        samplerDescriptor.tAddressMode = MTLSamplerAddressMode.repeat
        samplerDescriptor.minFilter = MTLSamplerMinMagFilter.nearest
        samplerDescriptor.magFilter = MTLSamplerMinMagFilter.nearest
        samplerDescriptor.normalizedCoordinates = true
        samplerState = device.makeSamplerState(descriptor: samplerDescriptor)
    }
    
    func buildRenderResources() {
        
        let textureLoader = MTKTextureLoader(device: device)
        let colorsImage = UIImage(named: "colormap")?.cgImage
        
        colorMap = try! textureLoader.newTexture(with: colorsImage!, options: nil)
        
        let vertexData: [Float] = [
            -1,  1, 0, 0,
            -1, -1, 0, 1,
            1, -1, 1, 1,
            1, -1, 1, 1,
            1,  1, 1, 0,
            -1,  1, 0, 0,
            ]
        vertexBuffer = device.makeBuffer(bytes: vertexData, length: vertexData.count * MemoryLayout.stride(ofValue: vertexData), options: [.storageModeShared] )
        
    }
    
    func buildRenderPipeline() {
        
        let fragmentProgram = defaultLibrary!.makeFunction(name: "simulation_fragment")
        let vertexProgram = defaultLibrary!.makeFunction(name: "simulation_vertex")
        
        let pipelineStateDescriptor = MTLRenderPipelineDescriptor()
        pipelineStateDescriptor.vertexFunction = vertexProgram
        pipelineStateDescriptor.fragmentFunction = fragmentProgram
        pipelineStateDescriptor.vertexDescriptor = makeVertexDescriptor()
        pipelineStateDescriptor.colorAttachments[0].pixelFormat = mtkView.colorPixelFormat
        
        // 3
        renderPipelineState =  try? self.device.makeRenderPipelineState(descriptor: pipelineStateDescriptor)
    }
    
    private func makeVertexDescriptor() -> MTLVertexDescriptor {
        let vertexDescriptor = MTLVertexDescriptor()
        vertexDescriptor.attributes[0].offset = 0;
        vertexDescriptor.attributes[0].bufferIndex = 0;
        vertexDescriptor.attributes[0].format = MTLVertexFormat.float2;
        vertexDescriptor.attributes[1].offset = MemoryLayout<Float>.size * 2;
        vertexDescriptor.attributes[1].bufferIndex = 0;
        vertexDescriptor.attributes[1].format = .float2;
        vertexDescriptor.layouts[0].stride = MemoryLayout<Float>.size * 4;
        vertexDescriptor.layouts[0].stepRate = 1;
        vertexDescriptor.layouts[0].stepFunction = .perVertex;
        return vertexDescriptor
    }
    
    
    func encodeComputeWork(`in` buffer: MTLCommandBuffer) {
        
        let commandEncoder = buffer.makeComputeCommandEncoder()
        
        
        // Create a compute command encoder with which we can ask the GPU to do compute work
        
        // For updating the game state, we divide our grid up into square threadgroups and
        // determine how many we need to dispatch in order to cover the entire grid
        let threadsPerThreadgroup = MTLSizeMake(32, 16, 1);
        let threadgroupCount = MTLSizeMake(Int(ceil(Float(gridSize.width) / Float(threadsPerThreadgroup.width))),
                                           Int(ceil(Float(gridSize.height) / Float(threadsPerThreadgroup.height))),
                                           1)
        
        
        commandEncoder.setComputePipelineState(copyPipelineState)
        commandEncoder.setTexture(textureQueue[3], at: 0)
        commandEncoder.setTexture(textureQueue[0], at: 1)
        commandEncoder.dispatchThreadgroups(threadgroupCount, threadsPerThreadgroup: threadsPerThreadgroup)

        commandEncoder.setComputePipelineState(copyPipelineState)
        commandEncoder.setTexture(textureQueue[0], at: 0)
        commandEncoder.setTexture(textureQueue[1], at: 1)
        commandEncoder.dispatchThreadgroups(threadgroupCount, threadsPerThreadgroup: threadsPerThreadgroup)
        
        
        if !activationVel.isEmpty {
            var bufferPos: [uint2] = []
            var bufferVel: [float2] = []
            
            for vel in activationVel {
                bufferPos.append(uint2(UInt32(Int(vel.0.x)), UInt32(Int(vel.0.y))))
                bufferVel.append(float2(Float(vel.1.x * 25), Float(vel.1.y * 25)))
            }
            activationVel.removeAll()
            
            commandEncoder.setComputePipelineState(addVelPipelineState)
            commandEncoder.setBytes(bufferPos, length: bufferPos.count * MemoryLayout.size(ofValue: bufferPos), at: 0)
            commandEncoder.setBytes(bufferVel, length: bufferVel.count * MemoryLayout.size(ofValue: bufferVel), at: 1)
            commandEncoder.setTexture(textureQueue[0], at: 0)
            commandEncoder.setTexture(textureQueue[1], at: 1)
            let threadsPerThreadgroupVel = MTLSizeMake(bufferPos.count, 1, 1);
            let threadgroupCountVel = MTLSizeMake(1, 1, 1);
            commandEncoder.dispatchThreadgroups(threadgroupCountVel, threadsPerThreadgroup: threadsPerThreadgroupVel)
        }
        
        if let point = activationPoints.first {
            var buffer: [uint2] = []
            for i in 0 ..< 20 {
                for j in 0 ..< 20 {
                    buffer.append(uint2(uint(point.x) + uint(i), uint(point.y) + uint(j)))
                }
            }
            let threadsPerThreadgroupLin = MTLSizeMake(400, 1, 1);
            let threadgroupCountLin = MTLSizeMake(1, 1, 1);
            let pointLength = 400 * MemoryLayout.size(ofValue: buffer)
            
            commandEncoder.setComputePipelineState(activationPipelineState)
            commandEncoder.setTexture(textureQueue[0], at: 0)
            commandEncoder.setTexture(textureQueue[1], at: 1)
            commandEncoder.setBytes(buffer, length: pointLength, at: 0)
            commandEncoder.dispatchThreadgroups(threadgroupCountLin, threadsPerThreadgroup: threadsPerThreadgroupLin)
            activationPoints.removeAll()
        }
        
        commandEncoder.setComputePipelineState(copyPipelineState)
        commandEncoder.setTexture(textureQueue[1], at: 0)
        commandEncoder.setTexture(textureQueue[0], at: 1)
        commandEncoder.dispatchThreadgroups(threadgroupCount, threadsPerThreadgroup: threadsPerThreadgroup)
        
        
        commandEncoder.setComputePipelineState(laplacePipelineState)
        commandEncoder.setTexture(textureQueue[0], at: 0)
        for i in 0 ..< 15 {
            commandEncoder.setTexture(textureQueue[1], at: 1 + (i % 2))
            commandEncoder.setTexture(textureQueue[2], at: 1 + ((i + 1) % 2))
            commandEncoder.dispatchThreadgroups(threadgroupCount, threadsPerThreadgroup: threadsPerThreadgroup)
        }
        
        commandEncoder.setComputePipelineState(preProjectPipelineState)
        commandEncoder.setTexture(textureQueue[1], at: 0)
        commandEncoder.setTexture(textureQueue[2], at: 1)
        
        
        commandEncoder.setComputePipelineState(linProjectPipelineState)
        for i in 0 ..< 15 {
            commandEncoder.setTexture(textureQueue[2], at: 1 + (i % 2))
            commandEncoder.setTexture(textureQueue[0], at: 1 + ((i + 1) % 2))
            commandEncoder.dispatchThreadgroups(threadgroupCount, threadsPerThreadgroup: threadsPerThreadgroup)
        }
      
        commandEncoder.setComputePipelineState(postPorojectPipelineState)
        commandEncoder.setTexture(textureQueue[2], at: 0)
        commandEncoder.setTexture(textureQueue[1], at: 1)
        
      
        
        commandEncoder.setComputePipelineState(advectPipelineState)
        commandEncoder.setTexture(textureQueue[1], at: 0)
        commandEncoder.setTexture(textureQueue[2], at: 1)
        commandEncoder.dispatchThreadgroups(threadgroupCount, threadsPerThreadgroup: threadsPerThreadgroup)
        
        
        commandEncoder.setComputePipelineState(preProjectPipelineState)
        commandEncoder.setTexture(textureQueue[2], at: 0)
        commandEncoder.setTexture(textureQueue[1], at: 1)
        
        
        commandEncoder.setComputePipelineState(linProjectPipelineState)
        for i in 0 ..< 15 {
            commandEncoder.setTexture(textureQueue[1], at: 1 + (i % 2))
            commandEncoder.setTexture(textureQueue[0], at: 1 + ((i + 1) % 2))
            commandEncoder.dispatchThreadgroups(threadgroupCount, threadsPerThreadgroup: threadsPerThreadgroup)
        }
        
        commandEncoder.setComputePipelineState(postPorojectPipelineState)
        commandEncoder.setTexture(textureQueue[1], at: 0)
        commandEncoder.setTexture(textureQueue[2], at: 1)
        
        
        
        commandEncoder.endEncoding()
        
        currentSimulationStateTexture = textureQueue[2]
        let toInsert = textureQueue.remove(at: 3)
        textureQueue.insert(toInsert, at: 0)
    }
    
    func encodeRenderWork(`in` buffer: MTLCommandBuffer) {
        
        guard let renderPassDescriptor = mtkView.currentRenderPassDescriptor else {
            return
        }
        
        let renderEncoder = buffer.makeRenderCommandEncoder(descriptor: renderPassDescriptor)
        renderEncoder.setRenderPipelineState(renderPipelineState)
        renderEncoder.setVertexBuffer(vertexBuffer, offset: 0, at: 0)
        renderEncoder.setFragmentTexture(currentSimulationStateTexture, at: 0)
        renderEncoder.setFragmentTexture(colorMap, at: 1)
        
        renderEncoder.drawPrimitives(type: .triangle, vertexStart: 0, vertexCount: 6)
        renderEncoder.endEncoding()
        buffer.present(mtkView.currentDrawable!)
    }
    
    func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize) {
        let scale = self.view.layer.contentsScale
        let drawableSize = mtkView.drawableSize
        let proposedGridSize = MTLSizeMake(Int(drawableSize.width / scale), Int(drawableSize.height / scale), 1);
        
        //    if (gridSize.width != proposedGridSize.width || gridSize.height != proposedGridSize.height) {
        gridSize = proposedGridSize;
        buildComputeResources()
        //    }
    }
    
    func buildComputeResources() {
        textureQueue.removeAll()
        currentSimulationStateTexture = nil
        let descriptor = MTLTextureDescriptor.texture2DDescriptor(pixelFormat: .rgba16Float, width: gridSize.width, height: gridSize.height, mipmapped: false)
        
        descriptor.usage = [.shaderRead, .shaderWrite]
        
        for i in 0 ..< kTextureCount {
            let texture = device.makeTexture(descriptor: descriptor)
            textureQueue.append(texture)
        }
    }
    
    public func draw(in view: MTKView) {
        inflightSemaphore.wait(timeout: DispatchTime.distantFuture)
        
        let commandBuffer = self.commandQueue.makeCommandBuffer()
        
        commandBuffer.addCompletedHandler { buffer in
            self.inflightSemaphore.signal()
        }
        encodeComputeWork(in: commandBuffer)
        encodeRenderWork(in: commandBuffer)
        commandBuffer.commit()
    }
}

extension SimulationViewController: UIGestureRecognizerDelegate {
    
}
